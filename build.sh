#!/bin/sh


echo "darwin building"
GOOS=darwin  GOARCH=amd64 go build -ldflags "-s -w" -o out/study_xxqg_darwin ./
echo "darwin done"

echo "darwin_arm64 building"
GOOS=darwin  GOARCH=arm64 go build -ldflags "-s -w" -o out/study_xxqg_darwin_arm64 ./
echo "darwin_arm64 done"

echo "win32 building"
GOOS=windows  GOARCH=386 go build -ldflags "-s -w" -o out/study_xxqg_win32.exe ./
echo "win32 done"

echo "win64 building"
GOOS=windows  GOARCH=amd64 go build -ldflags "-s -w" -o out/study_xxqg_win64.exe ./
echo "win64 done"

echo "linux64 building"
GOOS=linux  GOARCH=amd64 go build -ldflags "-s -w" -o out/study_xxqg_linux64 ./
echo "linux64 done"

echo "linux_arm64 building"
GOOS=linux  GOARCH=arm64 go build -ldflags "-s -w" -o out/study_xxqg_linux_arm64 ./
echo "linux_arm64 done"

echo "linux_arm building"
GOOS=linux  GOARCH=arm go build -ldflags "-s -w" -o out/study_xxqg_linux_arm ./
echo "linux_arm done"
