package push

import (
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"

	"github.com/johlanse/study_xxqg/conf"
	"github.com/johlanse/study_xxqg/utils"
)

func InitWecom() func(id string, kind, message string) {
	config := conf.GetConfig()

	sendMsg := func(data string) {
		var msg string = data
		if strings.Contains(data, "login.xuexi.cn") {
			msg = "可点击链接登录<a href=\\\"" + data + "\\\" >点击登录</a>"
		}

		if strings.Contains(data, "<br/>") {
			msg = strings.ReplaceAll(data, "<br/>", `\n`)
		}

		_, err := utils.GetClient().R().
			// SetFile("media", "qrcode.png").
			SetBodyJsonString(`{
				"msg": {
					"touser" : "` + config.Wecom.UID + `",
					"agentid" : 1000002,
					"msgtype": "text",
					"text": {
						"content": "` + msg + `"
					},
					"enable_id_trans": 0,
					"enable_duplicate_check": 1,
					"duplicate_check_interval": 10
				}
			}`).
			Post(config.Wecom.URL)
		if err != nil {
			log.Errorln(err.Error())
			return
		}
	}

	sendLoginQrMsg := func(data string) {
		_, err := utils.GetClient().R().
			SetFile("media", "xuexi.jpg").
			SetFile("qrcode", "qrcode.png").
			SetFormData(map[string]string{
				"msg": `{"touser":"` + config.Wecom.UID + `","agentid":1000002,"msgtype":"mpnews","mpnews":{"articles":[{"title":"点击查看登录二维码","thumb_media_id":"$$media_MEDIA_ID$$","author":"Author","content_source_url":"URL","content":"<div><p>使用下面二维码扫码登录</p><img src=\"https://qyapi.weixin.qq.com/cgi-bin/media/get?access_token=$$ACCESS_TOKEN$$&media_id=$$qrcode_MEDIA_ID$$\" /></div>","digest":"Digest description"}]},"safe":0,"enable_id_trans":0,"enable_duplicate_check":0,"duplicate_check_interval":1800}`,
			}).
			Post(config.Wecom.URL)
		if err != nil {
			log.Errorln(err.Error())
			return
		}
	}

	return func(id string, kind, message string) {
		message = strings.ReplaceAll(message, "\n", "<br/>")
		switch {
		case kind == "image":
			message = fmt.Sprintf("![](%v)", "data:image/png;base64,"+message)
			sendLoginQrMsg(message)
		case kind == "flush":
			if message != "" {
				sendMsg(message)
			}
		default:
			if log.GetLevel() == log.DebugLevel {
				sendMsg(message)
			}
		}
	}
}
